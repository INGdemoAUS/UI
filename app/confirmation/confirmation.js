'use strict';

angular.module('myApp.confirmation', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/confirmation', {
    templateUrl: 'confirmation/confirmation.html',
    controller: 'confirmationCtrl'
  });
}])

.controller('confirmationCtrl', [function() {

}]);