'use strict';

angular.module('myApp.purchase', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/purchase', {
    templateUrl: 'purchase/purchase.html',
    controller: 'purchaseCtrl'
  });
}])

.controller('purchaseCtrl', [function() {

}]);