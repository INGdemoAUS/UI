'use strict';

angular.module('myApp.trades', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/trades', {
    templateUrl: 'trades/trades.html',
    controller: 'tradesCtrl'
  });
}])

.controller('tradesCtrl', [function() {

}]);